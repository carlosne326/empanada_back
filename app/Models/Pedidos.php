<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pedidos extends Model
{
    use HasFactory;

    protected $table = 'pedidos';
    protected $primaryKey = 'id_pedido';
    public $timestamps = false;

    public static function getCarrito($idPedido)
    {
        $carrito = \DB::select('select * from v_carrito where id_pedido = :idPedido and cantidad > 0', ['idPedido' => $idPedido]);
        return $carrito;
    }

    public static function getPedidos($id_user = false)
    {
        if (!$id_user) {
            $pedidos = \DB::select('select * from v_pedidosAdmin');
        } else {
            $pedidos = \DB::select('select * from v_pedidosAdmin where idUsuario = :idUsuario', ['idUsuario' => $id_user]);
        }
        return $pedidos;
    }

    public static function getInfoPedido($idPedido)
    {
        $pedidos = \DB::select('select * from v_pedidosAdmin where idPedido = :idPedido', ['idPedido' => $idPedido]);
        return $pedidos[0];
    }
}
