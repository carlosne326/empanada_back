<?php

namespace App\Flow;
/**
 * Clase para Configurar el cliente
 * @Filename: Config.class.php
 * @version: 2.0
 * @Author: flow.cl
 * @Email: csepulveda@tuxpan.com
 * @Date: 28-04-2017 11:32
 * @Last Modified by: Carlos Sepulveda
 * @Last Modified time: 28-04-2017 11:32
 */
 
 
 
 class Config {

	const COMMERCE_CONFIG = array(
		"APIKEY" => "677F8FFF-0DB5-4DD5-A292-27L578A2FD57", // Registre aquí su apiKey
		"SECRETKEY" => "a9c4318b244d53e430f5a850e7fc99ed80e1df80", // Registre aquí su secretKey
		"APIURL" => "https://sandbox.flow.cl/api", // Producción EndPoint o Sandbox EndPoint
		"BASEURL" => "http://127.0.0.1:8000/api/flow/" //Registre aquí la URL base en su página donde instalará el cliente
	);
 	
	static function get($name) {
		if(!isset(self::COMMERCE_CONFIG[$name])) {
			throw new Exception("The configuration element thas not exist", 1);
		}
		return self::COMMERCE_CONFIG[$name];
	}
 }
