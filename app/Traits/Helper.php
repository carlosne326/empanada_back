<?php

namespace App\Traits;

use App\Models\Pedidos;

trait Helper {
    public function getCarrito($idPedido)
    {
        $carrito = Pedidos::getCarrito($idPedido);
        $total = 0;
        foreach ($carrito as $detalle) {
            $total += $detalle->subtotal;
        }
        
        return [
            'carrito' => $carrito,
            'total' => $total
        ];
    }

    public function getActions($estado)
    {
        $actions = [
            2 => [
                [
                    'idEstado' => 3,
                    'label' => 'Pagado',
                    'class' => 'primary'
                ],
                [
                    'idEstado' => 7,
                    'label' => 'Cancelar',
                    'class' => 'danger'
                ]
            ],
            3 => [
                [
                    'idEstado' => 4,
                    'label' => 'Aceptar',
                    'class' => 'primary'
                ],
                [
                    'idEstado' => 7,
                    'label' => 'Cancelar',
                    'class' => 'danger'
                ]
            ],
            4 => [
                [
                    'idEstado' => 5,
                    'label' => 'Despachado',
                    'class' => 'primary'
                ],
                [
                    'idEstado' => 7,
                    'label' => 'Cancelado',
                    'class' => 'danger'
                ]
            ],
            5 => [
                [
                    'idEstado' => 6,
                    'label' => 'Finalizado',
                    'class' => 'primary'
                ]
            ]
        ];

        if (array_key_exists($estado, $actions)) {
            return $actions[$estado];
        }

        return 'no-actions';
    }
}