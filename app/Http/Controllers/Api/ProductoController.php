<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Producto;
use Illuminate\Http\Request;
use App\Http\Requests\ProductoRequest;
use Illuminate\Support\Facades\Auth;

class ProductoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos = Producto::where('estado_producto', 1)->get();

        if ($productos) {
            return response()->json(['msg' => 'Productos obtenidos!', 'productos' => $productos], 200);
        }

        return response()->json(['message' => 'Error al obtener productos'], 500);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductoRequest $request)
    {
        $request->validated();

        $producto = new Producto();

        $producto->nombre_producto = $request->input('nombre');
        $producto->descripcion_producto = $request->input('descripcion');
        $producto->estado_producto = $request->input('estado');
        $producto->precio_producto = $request->input('precio');

        $res = $producto->save();

        if ($res) {
            return response()->json(['msg' => 'Producto insertado!'], 201);
        }

        return response()->json(['message' => 'Error al crear producto'], 500);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function show(Producto $producto)
    {
        return response()->json(['producto' => $producto], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function update(ProductoRequest $request, Producto $producto)
    {
        $request->validated();

        $producto->nombre_producto = $request->input('nombre');
        $producto->descripcion_producto = $request->input('descripcion');
        $producto->estado_producto = $request->input('estado');
        $producto->precio_producto = $request->input('precio');

        $res = $producto->save();

        if ($res) {
            return response()->json(['msg' => 'Producto actualizado!', 'producto' => $producto], 200);
        }

        return response()->json(['message' => 'Error al actualizar producto'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Producto $producto)
    {
        $res = $producto->destroy();

        if ($res) {
            return response()->json(['msg' => 'Producto eliminado!'], 201);
        }

        return response()->json(['message' => 'Error al eliminar producto'], 500);
    }

    public function getAll()
    {
        $productos = Producto::all();

        if ($productos) {
            return response()->json(['msg' => 'Productos obtenidos!', 'productos' => $productos], 200);
        }

        return response()->json(['message' => 'Error al obtener productos'], 500);
        
    }
}
