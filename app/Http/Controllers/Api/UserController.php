<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Persona;
use App\Models\Pedidos;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Traits\Helper;

class UserController extends Controller
{
    use Helper;

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['store']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$request->validated();

        $user = new User();

        $user->name = $request->input('nombre');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));

        $resU = $user->save();

        $persona = new Persona();

        $persona->idUsuario_persona = $user->id;
        $persona->rut_persona = $request->input('rut');
        $persona->nombre_persona = $request->input('nombre');
        $persona->direccion_persona = $request->input('direccion');
        $persona->telefono_persona = $request->input('telefono');

        $resp = $persona->save();

        if ($resU && $resp) {
            return response()->json(['msg' => 'Usuario registrado!'], 201);
        }

        return response()->json(['message' => 'Error al registrar usuario'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(UserRequest $user)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    public function load()
    {
        $user = auth()->user();
        $persona = Persona::firstWhere('idUsuario_persona', $user->id);
        $persona->uid = $user->id;
        $persona->nombre = explode(' ',trim($persona->nombre_persona))[0];
        $carritoPedido = Pedidos::where('idUsuario_pedido', $user->id)
            ->where('estado_pedido', 1)
            ->first();
        $carritoResponse = 'no data';
        if ($carritoPedido) {
            $carritoResponse = $this->getCarrito($carritoPedido->id_pedido);
        }
        $persona->carrito = $carritoResponse;

        return response()->json(['user' => $persona], 200);
    }

    public function getUserList()
    {
        $usuarios = Persona::getAll();

        return response()->json(['usuarios' => $usuarios], 200);
    }
}
