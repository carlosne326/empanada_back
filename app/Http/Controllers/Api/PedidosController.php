<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Pedidos;
use App\Models\DetallePedido;
use Illuminate\Http\Request;
use App\Traits\Helper;
use App\Flow\FlowApi;
use App\Flow\Config;

class PedidosController extends Controller
{
    use Helper;

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['confirmPayment']]);
    }
/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pedidos = Pedidos::getPedidos();

        if ($pedidos) {
            return response()->json(['msg' => 'Pedidos encontrados!', 'pedidos' => $pedidos ], 200);
        }

        return response()->json(['message' => 'Error al procesar carrito'], 500);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pedidos  $pedidos
     * @return \Illuminate\Http\Response
     */
    public function show(Pedidos $pedido)
    {
        return response()->json(['msg' => 'Pedidos encontrados!', 'pedido' => $pedido ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pedidos  $pedidos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pedidos $pedidos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pedidos  $pedidos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pedidos $pedidos)
    {
        //
    }

    public function addCart(Request $request)
    {
        $user = auth()->user();
        $idPedidoCarrito = 0;
        $idProducto = $request->input('idProducto');
        $insertFlag = true;

        //buscar carrito del usuario
        $carritoPedido = Pedidos::where('idUsuario_pedido', $user->id)
            ->where('estado_pedido', 1)
            ->first();

        // obtenemos el id del carrito, si no existe, creamos uno y obtenemos su id
        if ($carritoPedido) {
            $idPedidoCarrito = $carritoPedido->id_pedido;
        } else {
            $pedido = new Pedidos();
            $pedido->idUsuario_pedido = $user->id;
            $pedido->estado_pedido = 1;
            $pedido->fecha_pedido = \Carbon\Carbon::now()->timestamp;

            $res = $pedido->save();
            if ($res) {
                $idPedidoCarrito = $pedido->id_pedido;
            }
        }

        $carrito = DetallePedido::where('idPedido_detallep', $idPedidoCarrito)->get();

        if ($carrito) {
            foreach ($carrito as $detalle) {
                if ($detalle->idProducto_detallep == $idProducto) {
                    $detalle->cantidad_detallep++;
                    $detalle->save();
                    $insertFlag = false;
                }
            }
        }

        //return response()->json(['carrito' => $carrito, 'idp' => $idPedidoCarrito], 200);

        if ($insertFlag) {
            $detallep = new DetallePedido();
            $detallep->idPedido_detallep = $idPedidoCarrito;
            $detallep->idProducto_detallep = $idProducto;
            $detallep->cantidad_detallep = 1;
            $detallep->save();
        }

        $carritoResponse = $this->getCarrito($idPedidoCarrito);

        if ($carritoResponse) {
            return response()->json(['msg' => 'Carrito encontrado!', 'carro' => $carritoResponse], 200);
        }

        return response()->json(['message' => 'Error al procesar carrito'], 500);

    }

    public function getPaymentUrl(Request $request)
    {
        $user = auth()->user();
        $pedido = Pedidos::where('idUsuario_pedido', $user->id)
        ->where('estado_pedido', 1)
        ->first();
        $carrito = $this->getCarrito($pedido->id_pedido);
        $pedido->total_pedido = $carrito['total'];
        $pedido->estado_pedido = 2;
        $pedido->fecha_pedido = \Carbon\Carbon::now()->timestamp;
        if ($request->input('notas') != '') {
            $pedido->notas_pedido = $request->input('notas');
        }

        $pedido->save();

        //Prepara el arreglo de datos
        $params = array(
            "commerceOrder" => $pedido->id_pedido, 
            "subject" => "Pago de prueba",
            "currency" => "CLP",
            "amount" => $pedido->total_pedido,
            "email" => "carlosne326@gmail.com",
            "paymentMethod" => 1,
            "urlConfirmation" => Config::get("BASEURL") . "/confirm",
            "urlReturn" => "http://localhost:8080/#/resumenPedido",
        );
        //Define el metodo a usar
        $serviceName = "payment/create";

        try {
            // Instancia la clase FlowApi
            $flowApi = new FlowApi;
            // Ejecuta el servicio
            $response = $flowApi->send($serviceName, $params,"POST");
            //Prepara url para redireccionar el browser del pagador
            $redirect = $response["url"] . "?token=" . $response["token"];
            
            return response()->json([
                'msg' => 'Pedido actualizado!',
                'pedido' => $pedido,
                'redirect' => $redirect
            ], 200);
        } catch (Exception $e) {
            echo $e->getCode() . " - " . $e->getMessage();
        }

        return response()->json([
            'msg' => 'Pedido actualizado!',
            'pedido' => $pedido
        ], 200);
    }

    public function getFichaPedido(Request $request)
    {
        $idPedido = $request->input('idPedido');
        $pedido = Pedidos::getInfoPedido($idPedido);
        $detallePedido = Pedidos::getCarrito($idPedido);
        $actions = $this->getActions($pedido->idEstado);

        return response()->json([
            'msg' => 'Pedido encontrado!',
            'pedido' => $pedido,
            'detallePedido' => $detallePedido,
            'actions' => $actions
        ], 200);
    }

    public function confirmPayment(Request $request)
    {
        try {
            if(!isset($_POST["token"])) {
                throw new Exception("No se recibio el token", 1);
            }
            $token = filter_input(INPUT_POST, 'token');
            $params = array(
                "token" => $token
            );
            $serviceName = "payment/getStatus";
            $flowApi = new FlowApi();
            $response = $flowApi->send($serviceName, $params, "GET");
            
            //Actualiza los datos en su sistema
            //print_r($response);

            $pedido = new Pedidos();
            $pedido->notas_pedido = print_r($response, true);
            $pedido->save;
            
            
        } catch (Exception $e) {
            echo "Error: " . $e->getCode() . " - " . $e->getMessage();
        }
    }

    public function cambiaEstado(Request $request)
    {
        $idPedido = $request->input('idPedido');
        $estado = $request->input('estado');
        $pedido = Pedidos::find($idPedido);
        $pedido->estado_pedido = $estado;
        $pedido->save();

        $res = Pedidos::getInfoPedido($idPedido);

        $actions = $this->getActions($pedido->estado_pedido);

        return response()->json([
            'msg' => 'Pedido encontrado!',
            'pedido' => $res,
            'actions' => $actions
        ], 200);
    }

    public function removeCart(Request $request)
    {
        $user = auth()->user();
        $idPedidoCarrito = 0;
        $idProducto = $request->input('idProducto');

        //buscar carrito del usuario
        $carritoPedido = Pedidos::where('idUsuario_pedido', $user->id)
            ->where('estado_pedido', 1)
            ->first();

        $idPedidoCarrito = $carritoPedido->id_pedido;

        $carrito = DetallePedido::where('idPedido_detallep', $idPedidoCarrito)->get();

        if ($carrito) {
            foreach ($carrito as $detalle) {
                if ($detalle->idProducto_detallep == $idProducto) {
                    $detalle->cantidad_detallep--;
                    $detalle->save();
                }
            }
        }

        $carritoResponse = $this->getCarrito($idPedidoCarrito);

        if ($carritoResponse) {
            return response()->json(['msg' => 'Carrito encontrado!', 'carro' => $carritoResponse], 200);
        }

        return response()->json(['message' => 'Error al procesar carrito'], 500);
    }

    public function pedidosUsuario(Request $request)
    {
        $user = auth()->user();
        $pedidos = Pedidos::getPedidos($user->id);

        if ($pedidos) {
            return response()->json(['msg' => 'Pedidos encontrados!', 'pedidos' => $pedidos ], 200);
        }

        return response()->json(['message' => 'Error al procesar carrito'], 500);
    }
}
