<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(
    [
        'middleware' => 'api',
        'prefix' => 'auth'
    ], 
    function ($router) {
        Route::post('login', [\App\Http\Controllers\Api\AuthController::class, 'login'])->name('login');
        Route::post('logout', [\App\Http\Controllers\Api\AuthController::class, 'logout'])->name('logout');
        Route::post('refresh', [\App\Http\Controllers\Api\AuthController::class, 'refresh'])->name('refresh');
        Route::post('me', [\App\Http\Controllers\Api\AuthController::class, 'me'])->name('me');
    }
);

Route::apiResource('productos', App\Http\Controllers\Api\ProductoController::class)->middleware('api');
Route::post('productos/all', [App\Http\Controllers\Api\ProductoController::class, 'getAll'])->middleware('api');

Route::apiResource('usuarios', App\Http\Controllers\Api\UserController::class)->middleware('api');
Route::post('usuarios/load', [App\Http\Controllers\Api\UserController::class, 'load'])->middleware('api');
Route::post('usuarios/all', [App\Http\Controllers\Api\UserController::class, 'getUserList'])->middleware('api');

Route::apiResource('pedidos', App\Http\Controllers\Api\PedidosController::class)->middleware('api');
Route::post('pedidos/all', [App\Http\Controllers\Api\PedidosController::class, 'index'])->middleware('api');
Route::post('pedidos/addCart', [App\Http\Controllers\Api\PedidosController::class, 'addCart'])->middleware('api');
Route::post('pedidos/removeCart', [App\Http\Controllers\Api\PedidosController::class, 'removeCart'])->middleware('api');
Route::post('pedidos/paymentUrl', [App\Http\Controllers\Api\PedidosController::class, 'getPaymentUrl'])->middleware('api');
Route::post('pedidos/getFicha', [App\Http\Controllers\Api\PedidosController::class, 'getFichaPedido'])->middleware('api');
Route::post('pedidos/confirm', [App\Http\Controllers\Api\PedidosController::class, 'confirmPayment'])->middleware('api');
Route::post('pedidos/state', [App\Http\Controllers\Api\PedidosController::class, 'cambiaEstado'])->middleware('api');
Route::post('pedidos/user', [App\Http\Controllers\Api\PedidosController::class, 'pedidosUsuario'])->middleware('api');
